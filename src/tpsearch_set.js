
/*
tpsearch
Copyright (c) 2020, Stefan Naumann, tp
Licensed under GPL 3.0 or newer
tp Search was originally released under the MIT License
*/


/*
Stop words, in this case English and German
Stop words list from http://www.ranks.nl/stopwords
*/

var tp_stop_words = ["a", "above", "after", "again", "against", "all", "am", "an", "and", "any", "are", "aren't", "as", "at", "be", "because", "been", "before", "being", "below", "between", "both", "but", "by", "can't", "cannot", "could", "couldn't", "did", "didn't", "do", "does", "doesn't", "doing", "don't", "down", "during", "each", "few", "for", "from", "further", "had", "hadn't", "has", "hasn't", "have", "haven't", "having", "he", "he'd", "he'll", "he's", "her", "here", "here's", "hers", "herself", "him", "himself", "his", "how", "how's", "i", "i'd", "i'll", "i'm", "i've", "if", "in", "into", "is", "isn't", "it", "it's", "its", "itself", "let's", "me", "more", "most", "mustn't", "my", "myself", "no", "nor", "not", "of", "off", "on", "once", "only", "or", "other", "ought", "our", "ours", "ourselves", "out", "over", "own", "same", "shan't", "she", "she'd", "she'll", "she's", "should", "shouldn't", "so", "some", "such", "than", "that", "that's", "the", "their", "theirs", "them", "themselves", "then", "there", "there's", "these", "they", "they'd", "they'll", "they're", "they've", "this", "those", "through", "to", "too", "under", "until", "up", "very", "was", "wasn't", "we", "we'd", "we'll", "we're", "we've", "were", "weren't", "what", "what's", "when", "when's", "where", "where's", "which", "while", "who", "who's", "whom", "why", "why's", "with", "won't", "would", "wouldn't", "you", "you'd", "you'll", "you're", "you've", "your", "yours", "yourself", "yourselves", 
"aber","als","am","an","auch","auf","aus","bei","bin","bis","bist","da","dadurch","daher","darum","das","daß","dass","dein","deine","dem","den","der","des","dessen","deshalb","die","dies","dieser","dieses","doch","dort","du","durch","ein","eine","einem","einen","einer","eines","er","es","euer","eure","für","hatte","hatten","hattest","hattet","hier","hinter","ich","ihr","ihre","im","in","ist","ja","jede","jedem","jeden","jeder","jedes","jener","jenes","jetzt","kann","kannst","können","könnt","machen","mein","meine","mit","muß","mußt","musst","müssen","müßt","nach","nachdem","nein","nicht","nun","oder","seid","sein","seine","sich","sie","sind","soll","sollen","sollst","sollt","sonst","soweit","sowie","und","unser 	unsere","unter","vom","von","vor","wann","warum","was","weiter","weitere","wenn","wer","werde","werden","werdet","weshalb","wie","wieder","wieso","wir","wird","wirst","wo","woher","wohin","zu","zum","zur","über" ];


// Word replace

var tpsearch_replace = {'words': [

]};


// Weighting

var tpsearch_weight = {'weight': [

]};

var tpsearch_related = { 'Related' : [
    {"search": "testwort", "related": "raspberry pi"},
] };


// Stemming
var tpsearch_stem = {'words': [

]};


// Internal strings
var tpsearch_string_1 = 'Ohne Titel';
var tpsearch_string_2 = 'Zeige Resultate für';
var tpsearch_string_3 = 'Suche stattdessen nach';
var tpsearch_string_4 = '1 Resultat';
var tpsearch_string_5 = 'Resultate';
var tpsearch_string_8 = 'Nichts gefunden';
var tpsearch_string_9 = 'Häufige Worte werden ignoriert';
var tpsearch_string_10 = 'Verwandt';
var tpsearch_string_11 = 'Bitte wenigstens ein Zeichen eingeben';
var tpsearch_string_12 = 'Bitte wenigstens';
var tpsearch_string_13 = 'Zeichen oder mehr eingeben';
var tpsearch_string_14 = 'Sekunden';
var tpsearch_string_15 = 'Bild öffnen';
var tpsearch_string_16 = 'Gehe auf Seite ';
