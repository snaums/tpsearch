/*
tpsearch
Copyright (c) 2020 Stefan Naumann, Tipue (original creators)
This version is released under GPLv3 or newer
Tipue Search was originally released under the MIT License
*/

function setAttr ( x, attr, dflt ) {
    if ( typeof x[attr] === "undefined" ) {
        return dflt;
    } else {
        return x[attr];
    }
}

function hide ( obj ) {
    if ( !obj.classList.contains ( "hidden" )) {
        obj.classList.add ("hidden");
    }
}

function show ( obj ) {
    if ( obj.classList.contains ( "hidden" )) {
        obj.classList.remove("hidden");
    }
}

class tpsearch {
    constructor ( options ) {
        this.filter_string = setAttr ( options, "filter_string", ".filter_checkbox" );
        this.filter_misc_strings = setAttr ( options, "filter_misc", [ "None", "Sonstiges" ] );
        for ( var x = 0; x < this.filter_misc_strings.length; x++ ) {
            this.filter_misc_strings[x] = this.filter_misc_strings[0].toLowerCase ();
        }
    

        this.filter_boxes = document.querySelectorAll ( this.filter_string );

        this.searchBox_string = setAttr ( options, "searchBox", "#tipue_search_input" );
        this.searchBox = document.querySelector ( this.searchBox_string );

        this.outputBox_string = setAttr ( options, "outputBox", "#tipue_search_content" );
        this.outputBox = document.querySelector ( this.outputBox_string );

        this.show = setAttr ( options, "show", 10 );
        this.newWindow = setAttr ( options, "newWindow", false );
        this.contextBufer = setAttr ( options, "contextBuffer", 60 );
        this.contextLength = setAttr ( options, "contextLength", 60 );
        this.contextStart = setAttr ( options, "contextStart", 90 );
        this.debug = setAttr ( options, "debug", false );
        this.descriptiveWords= setAttr ( options, "descriptiveWords", 25 );
        this.highlightTerms= setAttr ( options, "highlightTerms",true );
        this.minimumLength= setAttr ( options, "minimumLength",3 );
        this.showContext= setAttr ( options, "showContext",true );
        this.showTitleCount= setAttr ( options, "showTitleCount",true );
        this.showURL= setAttr ( options, "showURL",true );
        this.wholeWords = setAttr ( options, "wholeWords",true );

        this.setup ();
    }

    // rediscover the boxes and DOM-elements via their intput string
    rediscover () {
        this.filter_boxes = document.querySelectorAll ( this.filter_string );
        this.outputBox = document.querySelector ( this.outputBox_string );
        this.searchBox = document.querySelector ( this.searchBox_string );
    }

    // sets up event listeners
    setup () {
        tp = this;
        this.tipue_search_w = "";
        if (this.newWindow) {
            this.tipue_search_w = ' target="_blank"';
        }

        // event listener for the search Box
        this.searchBox.addEventListener ( "keyup", function (event) {
            // ENTER press
            if (event.keyCode == "13") {
                event.preventDefault();
            }
            tp.doTpSearch(0, true);
        });

        // on every change of the filter boxes
        for ( var i = 0; i < this.filter_boxes.length; i++ ) {
            this.filter_boxes[i].addEventListener ( "change", function (event) {
                tp.doTpSearch(0, true);
            });
        }

        // is a search term attached as hash-parameter?
        if ( location.hash.length > 0 ) {
            this.value = decodeURI(location.hash.substr(1))
        }

        if (this.searchBox.value.length > 0) {
            tp.doTpSearch(0, true);
        }
    }

    // get a list of filters
    getFilter() {
        var filter = {};
        var boxes = document.querySelectorAll(this.filter_string);

        for (var i = 0; i < boxes.length; i++) {
            var b = boxes[i];
            if (typeof filter[b.dataset.type] === "undefined") {
                filter[b.dataset.type] = [];
            }
            if (b.checked == true) {
                // if checked add the filter
                filter[b.dataset.type].push(b.dataset.name);
            }
        } 

        return filter;
    }

	// returns false, if the page is filtered out
	applyFilter ( page, filter ) {
		var k = Object.keys ( filter );
		var state = {};
		for ( var ki = 0; ki < k.length; ki++ ) {
			var key = k[ki];
			state[key] = false;

			// nothing selected in a category -> everything unset passes
			if ( filter[key].length == 0 ) {
				if ( typeof( page[key] ) !== "string" ) 
					state[key] = true;
			} else if ( typeof ( page[key] ) === "string" && page[key].length > 0 ) {
				for ( var x = 0; x < filter[key].length; x++ ) {
					if ( page[key].toLowerCase() == filter[key][x] ) {
						state[key] = true;
						break;
					}
				}
			} else {
				for ( var x = 0; x < filter[key].length; x++ ) {
                    if ( this.filter_misc_strings.includes ( filter[key][x] ) ) {
						state[key] = true;
						break;
                    }
				}
			}
		}

		for ( var ki = 0; ki < k.length; ki++ ) {
			if ( state[k[ki]] == false ) {
				return false;
			}
		}
		return true;
	}

    // do the search from start to finish
    doTpSearch ( start, replace ) {
        var found = this.getTpSearch ( replace );
        var repl = found["replace"];
        var d = found["d"];
        var d_r = found["d_r"];
        var found = found["found"];
        var pag   = this.pagination ( found, replace, start );
        this.doOutput ( found, pag, start, d, d_r );
    }
    
    // do the searching
    // returns an object:
    //   - .found -> list of sorted (by score) found articles
    //   - .replace
    //   - .d
    //   - .d_r
    getTpSearch(replace) {
        hide ( this.outputBox );

        var tipuesearch_t_c = 0;
        var tipuesearch_in = tipuesearch;

        var out = "";
        var show_replace = false;
        var show_stop = false;
        var standard = true;
        var c = 0;
        var found = [];

        var filter = this.getFilter();

        var d_o = this.searchBox.value;
        d_o = d_o.replace(/\+/g, " ").replace(/\s\s+/g, " ").trim();

        var d = d_o.toLowerCase();

        if ((d.match('^"') && d.match('"$')) || (d.match("^'") && d.match("'$"))) {
          standard = false;
        }

        var d_w = d.split(" ");
        if (standard) {
          d = "";
          for (var i = 0; i < d_w.length; i++) {
            var a_w = true;
            for (var f = 0; f < tpsearch_stop_words.length; f++) {
              if (d_w[i] == tpsearch_stop_words[f]) {
                a_w = false;
                show_stop = true;
              }
            }
            if (a_w) {
              d = d + " " + d_w[i];
            }
          }
          d = d.trim();
          d_w = d.split(" ");
        } else {
          d = d.substring(1, d.length - 1);
        }

        if (d.length >= this.minimumLength) {
          if (standard) {
            if (replace) {
              var d_r = d;
              for (var i = 0; i < d_w.length; i++) {
                for (var f = 0; f < tpsearch_replace.words.length; f++) {
                  if (d_w[i] == tpsearch_replace.words[f].word) {
                    d = d.replace(d_w[i], tpsearch_replace.words[f].replace_with);
                    show_replace = true;
                  }
                }
              }
              d_w = d.split(" ");
            }

            var d_t = d;
            for (var i = 0; i < d_w.length; i++) {
              for (var f = 0; f < tpsearch_stem.words.length; f++) {
                if (d_w[i] == tpsearch_stem.words[f].word) {
                  d_t = d_t + " " + tpsearch_stem.words[f].stem;
                }
              }
            }
            d_w = d_t.split(" ");

			// iterate over list of pages
            for (var i = 0; i < tipuesearch_in.pages.length; i++) {
				// apply filters
				if ( !this.applyFilter ( tipuesearch_in.pages[i], filter ) ) {
					continue;
				}

              var score = 0;
              var s_t = tipuesearch_in.pages[i].text;
              for (var f = 0; f < d_w.length; f++) {
                var pat = new RegExp(d_w[f], "gi");
                if (tipuesearch_in.pages[i].title.search(pat) != -1) {
                  var m_c = tipuesearch_in.pages[i].title.match(pat).length;
                  score += 20 * m_c;
                }
                if (tipuesearch_in.pages[i].text.search(pat) != -1) {
                  var m_c = tipuesearch_in.pages[i].text.match(pat).length;
                  score += 20 * m_c;
                }

                if (this.highlightTerms) {
                  if (this.highlightEveryTerm) {
                    var patr = new RegExp("(" + d_w[f] + ")", "gi");
                  } else {
                    var patr = new RegExp("(" + d_w[f] + ")", "i");
                  }
                }

                if (tipuesearch_in.pages[i].url.search(pat) != -1) {
                  score += 20;
                }

                if (score != 0) {
                  for (var e = 0; e < tpsearch_weight.weight.length; e++) {
                    if (tipuesearch_in.pages[i].url == tpsearch_weight.weight[e].url) {
                      score += tpsearch_weight.weight[e].score;
                    }
                  }
                }

                if (d_w[f].match("^-")) {
                  pat = new RegExp(d_w[f].substring(1), "i");
                  if (tipuesearch_in.pages[i].title.search(pat) != -1 || tipuesearch_in.pages[i].text.search(pat) != -1 || tipuesearch_in.pages[i].tags.search(pat) != -1) {
                    score = 0;
                  }
                }
              }

              if (score != 0) {
                found.push(
                    this.foundNode ( score, s_t, tipuesearch.pages[i] )
                );
                c++;
              }
            }
          } else {
            for (var i = 0; i < tipuesearch_in.pages.length; i++) {
				// apply filters
				if ( !this.applyFilter ( tipuesearch_in.pages[i], filter ) ) {
					continue;
				}

              var score = 0;
              var s_t = tipuesearch_in.pages[i].text;
              var pat = new RegExp(d, "gi");
              if (tipuesearch_in.pages[i].title.search(pat) != -1) {
                var m_c = tipuesearch_in.pages[i].title.match(pat).length;
                score += 20 * m_c;
              }
              if (tipuesearch_in.pages[i].text.search(pat) != -1) {
                var m_c = tipuesearch_in.pages[i].text.match(pat).length;
                score += 20 * m_c;
              }

              if (this.highlightTerms) {
                if (this.highlightEveryTerm) {
                  var patr = new RegExp("(" + d + ")", "gi");
                }
              }

              if (tipuesearch_in.pages[i].url.search(pat) != -1) {
                score += 20;
              }

              if (score != 0) {
                for (var e = 0; e < tpsearch_weight.weight.length; e++) {
                  if (tipuesearch_in.pages[i].url == tpsearch_weight.weight[e].url) {
                    score += tpsearch_weight.weight[e].score;
                  }
                }
              }

              if (score != 0) {
                found.push(
                    this.foundNode ( score, s_t, tipuesearch.pages[i] )
                );
                c++;
              }
            }
          }
        }
        
        // sort found by score
        found.sort(function (a, b) {
            return b.score - a.score;
        });
        return { found: found, replace: show_replace, d: d, d: d_r };
    }

    // return a node for the found-list
    foundNode ( score, s_t, page ) {
        return {
            score: score,
            title: page.title,
            desc: s_t,
            url: page.url,
            category: page.category,
            date: page.date,
            thumbnail: page.thumbnail,
        }
    }

    // return html-code for the pagination-elements
    pagination ( found, replace, start ) {
        var pagination = "";
        if (found.length > this.show) {
            var pages = Math.ceil(found.length / this.show);
            var page = start / this.show;
            pagination += '<div id="pagination" style="text-align:center;margin-bottom: 10px">';

            // prev
            if (start > 0) {
                pagination += '<span class="pagination_element prev"><a data-id="' + (start - this.show) + "_" + replace + '">«</a></span>';
            } else {
                pagination += '<span class="pagination_element_disabled">«</span>';
            }

            // middle part
            if (page <= 2) {
                var p_b = pages;
                if (pages > 3) {
                    p_b = 3;
                }
                for (var f = 0; f < p_b; f++) {
                    if (f == page) {
                        pagination += '<span class="pagination_element pagination_element_active">' + (f + 1) + "</span>";
                    } else {
                        pagination += '<span class="pagination_element"><a data-id="' + f * this.show + "_" + replace + '">' + (f + 1) + "</a></span>";
                    }
                }
            } else {
                var p_b = page + 2;
                if (p_b > pages) {
                    p_b = pages;
                }
                for (var f = page - 1; f < p_b; f++) {
                    if (f == page) {
                        pagination += '<span class="pagination_element pagination_element_active">' + (f + 1) + "</span>";
                    } else {
                        pagination += '<span><a class="tp_search_foot_box" data-id="' + f * this.show + "_" + replace + '">' + (f + 1) + "</a></span>";
                    }
                }
            }

            // next
            if (page + 1 != pages) {
                pagination += '<span class="next pagination_element"><a data-id="' + (start + this.show) + "_" + replace + '">»</a></span>';
            } else {
                pagination += '<span class="next pagination_element_disabled">»</span>';
            }

            pagination += "</div>";
        }

        return pagination;
    }

    // return output code, with pagination and result-html
    doOutput ( found, pagination, start, d, d_r ) {
        var c = found.length;
        var out = "";
        if (c != 0) {
            if (c == 1) {
                out += '<div id="tp_search_results_count">' + tpsearch_string_4 + "</div>";
            } else {
                var c_c = c.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                out += '<div id="tp_search_results_count">' + c_c + " " + tpsearch_string_5 + "</div>";
            }

            out += pagination;
            var l_o = 0;
            for (var i = 0; i < found.length; i++) {
                if (l_o >= start && l_o < this.show + start) {
                    var t = found[i].desc;
                    var t_d = "";
                    var t_w = t.split(" ");
                    if (t_w.length < this.descriptiveWords) {
                        t_d = t;
                    } else {
                        for (var f = 0; f < this.descriptiveWords; f++) {
                            t_d += t_w[f] + " ";
                        }
                    }
                    t_d = t_d.trim(t_d);
                    if (t_d.charAt(t_d.length - 1) != ".") {
                        t_d += " ...";
                    }
                    f = found[i];
                    out += '<article class="post-preview"><div class="post-inner post-hover"><div class="post-thumbnail">';
                    if (f.thumbnail.length > 0) {
                        out += '<a href="' + f.url + '" title="' + f.title + '" class="hover_img"><div style="background-image:url(\'' + f.thumbnail + '\')" class="thumb-medium"></div></a></div>';
                    }

                    if (f.category.length > 0) {
                        out += '<div class="post-meta group"><span class="post-category"><a href="/category/' + f.category + '.html" rel="category tag">' + f.category + '</a></span><span class="post-date"><time class="published updated">' + f.date + "</time></span></div>";
                    }

                    out += '<a href="' + f.url + '" rel="bookmark" title="' + f.title + '"><h2 class="post-title entry-title">' + f.title + '</h2></a><div class="entry excerpt entry-summary">' + t_d + '<div class="readmore"><a href="' + f.url + '" title="' + f.title + '">[weiter lesen]</a></div></div></div></article>';
                }
                l_o++;
            }
            out += pagination;
        } else {
            out += '<div id="tp_search_warning">' + tpsearch_string_8 + "</div>";
        }

        this.outputBox.innerHTML = out;
        show ( this.outputBox );

        var pg = document.querySelectorAll(".pagination_element");
        for ( var i = 0; i < pg.length; i++ ) {
            pg[i].addEventListener( "click", function () {
                if (this.children.length > 0) {
                    var id_v = this.children[0].dataset["id"];
                    var id_a = id_v.split("_");

                    doTpSearch(parseInt(id_a[0]), id_a[1]);
                }
            });
        }
    }
}

